# Bboss development

This repository is the entry point for early adopters and contributors who want to try and 
work with Bboss before its availability on pip.

It represents the user's project directory as it will be created by the generate command.

Fork this repository to create your own project.

## System requirements
Bboss is designed to run on Linux Systems, it has been tested on Ubuntu Server 18.04 and 
Ubuntu Mate 18.04 only.

**Hardware :**
- Processor : Intel core i5 or i7 (recommended)
- Ram : 16GB or 32GB (recommended)
- Hard disk space : 20GB or 30GB (recommended) 

**Ubuntu Server 18.04 :**
- pyhton 3.6.7
- git 2.17.1

**Other required dependecies :**
- docker 18.09.0 (docker-ce=5:18.09.0\~3-0~ubuntu-bionic)<br>
  [Get Docker CE for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)<br>
  [Post-installation steps for Linux](https://docs.docker.com/install/linux/linux-postinstall/)

## Docker Installation (Ubuntu 18.04)
**1. Install packages to allow apt to use a repository over HTTPS**
```bash
sudo apt-get update && \
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```
**2. Add and verify Docker’s official GPG key**

Verify that you now have the key with the fingerprint 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88.
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && 
sudo apt-key fingerprint 0EBFCD88 2>&1 | grep '9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88'
```
**3. Install Docker CE**

```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" && \
sudo apt-get update && 
sudo apt-get install -y docker-ce=5:18.09.0~3-0~ubuntu-bionic
```

**3. Post-installation steps for Linux**

```bash
sudo groupadd docker
sudo usermod -aG docker $USER
```
Restart your terminal session to apply changes.
## bboss-dev Installation
**1 - Install python virtualenv**
```bash
sudo apt-get install -y virtualenv
```
**2 - Get the sources**
```bash
git config --global credential.helper "cache --timeout=3600" && \
git clone --recursive https://gitlab.com/bboss/bboss-dev.git
```
## Start on your local computer
**1 - Start development environment (virtual env)**
```bash
cd bboss-dev && \
source dev/install
```
**2 - Configure and run your network infrastructure.**
```bash
bboss menuconfig
bboss up
```

**3 - Modify your /etc/hosts**

```bash
sudo nano /etc/hosts
```

Add infrastructure hosts :
```bash
# bboss.localhost
127.0.0.1 bboss.localhost

127.0.0.1 adminer.localhost
127.0.0.1 ldapadm.localhost
127.0.0.1 nexus.localhost
127.0.0.1 pgadmin.localhost
127.0.0.1 phpmyadm.localhost
127.0.0.1 portainer.localhost
127.0.0.1 sentry.localhost

127.0.0.1 gitlab.localhost
127.0.0.1 jenkins.localhost
127.0.0.1 mantis.localhost
127.0.0.1 readdocs.localhost
127.0.0.1 sonar.localhost

127.0.0.1 nxtcloud.localhost
127.0.0.1 odoo.localhost
127.0.0.1 openproj.localhost
127.0.0.1 redmine.localhost
```

**4 - Use your new infrastructure**
```bash
firefox http://bboss.localhost
```

